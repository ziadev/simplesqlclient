/* 
 * Copyright 2019 Zia Consulting
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Simple JDBC SQL client. The only jar dependency is a JDBC driver.  
 * Takes the path to a connection configuration properties file e.g. alfresco-global.properties as an argument.
 * JDBC driver is specified in the classpath.
 * Example command line for running Alfresco DB connection parameters:
 * java -classpath /opt/alfresco/tomcat/lib/postgresql-9.4.1212.jar:. SimpleSQLClient /opt/alfresco/tomcat/shared/classes/alfresco-global.properties
 */

import java.sql.*;
import java.io.*;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleSQLClient {
	
	// Quit commands
	private static final String CMD_QUIT = "quit";
	
	// Set options commands
	private static final String CMD_SET = "set";
	private static final String OPT_WIDTH = "width";
	private static final String OPT_PROMPT = "prompt";
	private static final String OPT_STRIP = "strip";
	private static final String OPT_DELIMITER = "delimiter";
	private static final String OPT_ERROREXIT = "errorexit";
	private static final String OPT_ECHOSTMT = "echostmt";
	private static final String OPT_COMMENT = "comment";

	// Show help
	private static final String CMD_HELP = "help";
	
	private static final String NEWLINE = System.getProperty("line.separator");

	private Connection conn = null;
	private Statement stmt = null;
	private Properties props = null;
	private Character commandDelimiter;
	private Boolean stripDelimiter;
	private String commandPrompt;
	private Integer maxColumnWidth;
	private Boolean exitOnError;
	private Boolean echoStmt = false;
	private String commentDelimiter = "";
	private String continuePrompt = "--  ";
	
	/*
	 * The name and parameters say it all.
	 */
	protected void closeDBResources(Connection conn, Statement stmt, ResultSet rslt) {
		try {
			if (rslt != null) {
				rslt.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			consolePrintln("Exception closing resources " + e.getMessage());
		}

	}
	
	/*
	 * Print with no newline.
	 */
	protected static void consolePrint(String printMe) {
		System.out.print(printMe);
	}
	
	/*
	 * Print with newline.
	 */
	protected static void consolePrintln(String printMe) {
		consolePrint(printMe + NEWLINE);
	}
	
	/*
	 * Read one or more lines from user and create single line statement or command.
	 */
	protected String getUserInput(BufferedReader in) throws IOException {
		StringBuilder userInput = new StringBuilder();
		String line;
        consolePrint(commandPrompt + " ");
        // Read lines from stdin until we until we hit a statement delimiter OR see a line starting with a command.
        while ((line = in.readLine()) != null) {
        	line = line.trim();
        	// Ignore empty lines it'll just mess up the continuation prompt.
        	if(line.length() == 0) {
        		continue;
        	}
        	else {
        		int commentDex = -1;
        		// If we see a comment strip it out. Note that these are single line and may 
        		// occur after a statement to end of line. We don't do block comments.
        		if(commentDelimiter != null && commentDelimiter.length() > 0 && (commentDex = line.indexOf(commentDelimiter)) > -1) {
        			line = line.substring(0, commentDex).trim();
        			// If the line was just a comment go back and read another line.
        			if(line.length() == 0) {
        				continue;
        			}
        		}
        		// If multi-line input separate with a space and append the current line.
        		if(userInput.length() > 1) {
        			userInput.append(" ");
        		}
        		// Do we have a termination character commandDelimiter or is this the first
        		// line and we have an internal command?
        		if(line.charAt(line.length() - 1) != commandDelimiter && 
        				!(line.toLowerCase().startsWith(CMD_QUIT)) &&
        				!(line.toLowerCase().startsWith(CMD_SET)) &&
        				!(line.toLowerCase().startsWith(CMD_HELP)))
        		{
        			userInput.append(line);
        			// Prompt for next line.
        			consolePrint(continuePrompt);
        		}
        		// End of input or an internal command.
        		else {
        			userInput.append(line);
        			break;
        		}
        	}
        }
        
        // EOF?  
        if(line != null) {
        	// We've processed one or more lines in the read loop above to a single line. 
        	// Check whether we should strip command delimiters.
        	if(stripDelimiter) {
        		int dexDelimiter = userInput.lastIndexOf(commandDelimiter + "");
        		if(dexDelimiter > -1) {
        			userInput.setCharAt(dexDelimiter, ' ');
        		}
        	}
        	return userInput.toString().trim();
        }
        return null;
	}
	
	/*
	 * expand placeholders of the format ${name}.
	 * Example, in the properties file.
	 * db.host=localhost
	 * db.name=alfresco
	 * db.port=3306
	 * db.url=jdbc:mysql://${db.host}:${db.port}/${db.name}?useUnicode=yes&characterEncoding=UTF-8
	 * Calling with the key string of db.url will expand parameters db.host, db.port, db.name for output. 
	 * Returns the string 
	 * jdbc:mysql://localhost:3306/alfresco?useUnicode=yes&characterEncoding=UTF-8
	 */
	private String getExpandedProperty(String key, Properties props, String defVal) {
		// Get the value of the requested property
		String value = props.getProperty(key, defVal);
		if(value != null) {
			// Look for the first replacement if it exists.
			final Pattern p = Pattern.compile("\\$\\{(.*?)\\}");
		    Matcher m = p.matcher(value);
		    while(m.find()) {
		    	String replaceKey = m.group(1);
		    	String replaceValue = props.getProperty(replaceKey, "");
		    	value = m.replaceFirst(replaceValue);
		    	m.reset(value);
		    }
		}
		return value;
	}
	
	/*
	 * When generating tables from SQL execution results create a string value for display that is space padded to 
	 * at least the column display size or truncated if wider than our max width option. 
	 */
	private String getPaddedValue(String value, int columnDisplaySize, String padChar) {
		String out = "";
		if(value != null) {
			// Too wide? Clip it.
			if(value.length() > maxColumnWidth) {
				out = value.substring(0, maxColumnWidth);
			}
			// Pad out to column display width.
			else {
				int padLen = columnDisplaySize - value.length();
				padLen = (padLen < 0 ? 0 : padLen);
				out = value + new String(new char[padLen]).replace("\0", padChar);
			}
		}
		return out;
	}
	
	/*
	 * Get a JDBC SQL statement object.
	 * Opens the connection if required and detects closed objects.
	 */
	protected Statement getStatement() throws SQLException, ClassNotFoundException {
		if(stmt == null || stmt.isClosed()) {
			if(conn == null || conn.isValid(500) == false) {
				Class.forName(props.getProperty("db.driver"));
				conn = DriverManager.getConnection(getExpandedProperty("db.url", props, null), props.getProperty("db.username"), props.getProperty("db.password"));
			}
			stmt = conn.createStatement();
		}
		return stmt;
	}
	
	/*
	 * Read the properties file from pathname passed on command line and set 
	 * any options.
	 */
	protected void initProps(String propsFile) throws Exception, IOException {
		if(propsFile == null && propsFile.length() < 1) {
			printHelp();
			throw new Exception("Missing properties filename.");
		}
		
		// Read our properties file.
		InputStream istream = new FileInputStream(propsFile);
		props = new Properties();
		props.load(istream);
		istream.close();
		
		// Set our options from the properties. These are short and simple so no expansion.
		commandDelimiter = props.getProperty("command.delimiter", ";").charAt(0);
		stripDelimiter = props.getProperty("strip.delimiter", "true").equals("true");
		commandPrompt = props.getProperty("command.prompt", "--> "); 
		maxColumnWidth = Integer.parseInt(props.getProperty("max.column.width", "100")); 
		exitOnError = props.getProperty("exit.on.error", "false").equals("true");
	}
	
	/*
	 * Look for a command substring 'has' at the beginning of the string 'in'.
	 * Return true if 'has' found at beginning of the trimmed string.
	 * -1 if not found.
	 */
	private Boolean isCommand(String has, String in) {
	    int start = -1;
	    if(has != null && in != null) {
	    	final Pattern p = Pattern.compile(in, Pattern.CASE_INSENSITIVE);
	    	Matcher m = p.matcher(has.trim());
	    	if( m.find() ) {
	    		start = m.start();
	    	};
	    }
	    return start == 0;
	}
	
	/*
	*
	*/
	public static void main(String[] args) {
		SimpleSQLClient client = new SimpleSQLClient();
		System.exit(client.run(args));
	}
	
	/*
	* Run a new instance.
	* Processes user input until we're told to stop. 
	*/
	private int run(String[] args) {
		String userInput = null;
        int exitCode = 0;
		try {
			initProps(args[0]);
			
			// Read lines from stdin and run as internal commands or SQL statements.
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			while(!(userInput = getUserInput(in)).equals(CMD_QUIT)) {
				if(userInput.length() > 0) {
					// Setting a property?
					if(isCommand(userInput, CMD_SET)) {
						setOption(userInput);
					}
					// Need help?
					else if(isCommand(userInput, CMD_HELP)) {
						printHelp();
					}
					// Everything else is assumed to be SQL.
					else {
						Date start = new Date();
						try {
							runSQL(userInput);
						}
						catch (ClassNotFoundException ecnf) {
							consolePrintln("Class not found, most likely missing a jdbc driver in the '-classpath' argument to java." + ecnf.getLocalizedMessage() + NEWLINE + (ecnf.getCause() == null ? "" : NEWLINE + ecnf.getCause()));
							ecnf.printStackTrace();
							consolePrintln("Exiting on severe error");
							break;
						}
						catch (Exception e) {
							consolePrintln("Exception running sql " + e.getLocalizedMessage() + NEWLINE + (e.getCause() == null ? "" : NEWLINE + e.getCause()));
							// e.printStackTrace();
							if(exitOnError) { 
								consolePrintln("Exiting on sql error");
								exitCode = 2;
								break;
							}
						}
						Date end = new Date(); 
						consolePrintln("Statement execution in " + (end.getTime() - start.getTime()) + " milliseconds.");
					}
				}
			}
		}
		catch(IOException ioe) {
			consolePrintln("Exception opening properties " + args[0] + " " + ioe.getMessage());
			ioe.printStackTrace();
			exitCode = 1;
		}
		catch(Exception e) {
			consolePrintln("General exception " + e.getMessage());
			e.printStackTrace();
			exitCode = 3;
		}
		finally {
			closeDBResources(conn, null, null);
		}
		return exitCode;
	}
	
	/*
	 * Command line set option command handler.
	 */
	private void setOption(String command) {
		String[] setVals = command.split(" ");
		if(setVals.length > 1) { 
			if(isCommand(setVals[1], OPT_DELIMITER)) {
				if(setVals.length > 2) {
					commandDelimiter = setVals[2].charAt(0);
				}
				consolePrint(formatOption(OPT_DELIMITER, commandDelimiter.toString()));
			}
			else if(isCommand(setVals[1], OPT_STRIP)) {
				if(setVals.length > 2) {
					stripDelimiter = isCommand(setVals[2], "true");
				}
				consolePrint(formatOption(OPT_STRIP, stripDelimiter.toString()));
			}
			else if(isCommand(setVals[1], OPT_PROMPT)) {
				if(setVals.length > 2 && setVals[2].length() > 0) {
					commandPrompt = setVals[2];
				}
				consolePrint(formatOption(OPT_PROMPT, commandPrompt));
			}
			else if(isCommand(setVals[1], OPT_WIDTH)) {
				if(setVals.length > 2 && setVals[2].length() > 0) {
					maxColumnWidth = Integer.parseInt(setVals[2]);
				}
				consolePrint(formatOption(OPT_WIDTH, maxColumnWidth.toString()));
			}
			else if(isCommand(setVals[1], OPT_ERROREXIT)) {
				if(setVals.length > 2) {
					exitOnError = isCommand(setVals[2], "true");
				}
				consolePrint(formatOption(OPT_ERROREXIT, exitOnError.toString()));
			}
			// Only available from the command line for sql scripts
			else if(isCommand(setVals[1], OPT_ECHOSTMT)) {
				if(setVals.length > 2) {
					echoStmt = isCommand(setVals[2], "true");
				}
				consolePrint(formatOption(OPT_ECHOSTMT, echoStmt.toString()));
			}
			// Only available from the command line for sql scripts
			else if(isCommand(setVals[1], OPT_COMMENT)) {
				if(setVals.length > 2) {
					commentDelimiter = setVals[2];
				}
				consolePrint(formatOption(OPT_COMMENT, commentDelimiter));
			}
			else {
				consolePrintln("Invalid option");
			}
		}
		// With no option specified print all current settings.
		else {
			consolePrint(
				"Option values:" + NEWLINE + 
				formatOption(OPT_DELIMITER, commandDelimiter.toString()) +
				formatOption(OPT_STRIP, stripDelimiter.toString()) +
				formatOption(OPT_PROMPT, commandPrompt) +
				formatOption(OPT_WIDTH, maxColumnWidth.toString()) +
				formatOption(OPT_ERROREXIT, exitOnError.toString()) +
				formatOption(OPT_ECHOSTMT, echoStmt.toString()) +
				formatOption(OPT_COMMENT, commentDelimiter));			
		}
	}
	
	/*
	 * Print a current option setting. 
	 */
	protected String formatOption(String optName, String optValue) { 
		return ("\t" + optName + " set to " + optValue + NEWLINE);
	}
	
	/*
	 * Display help.
	 */
	protected void printHelp() {
		consolePrint(
			"<SQL>" + commandDelimiter + " to execute statement." + NEWLINE +
			CMD_QUIT + " to exit the application." + NEWLINE +
			CMD_SET + " [<command> <value>] If no arguments prints current settings." + NEWLINE +
			"\t" + OPT_DELIMITER + " <character> Terminates commands in console, change if submitting multiple statements and delimiter collides." + NEWLINE +
			"\t" + OPT_STRIP + " [true | false] Strip the delimiter before sending the command string to the DBMS." + NEWLINE +
			"\t" + OPT_PROMPT + " <string> Set the command prompt to string." + NEWLINE +
			"\t" + OPT_WIDTH + " <int> Set the maximum column display width." + NEWLINE +
			"\t" + OPT_ERROREXIT + " [true | false] Exit on statement execution error." + NEWLINE
		);
	}
	
	/*
	 * Print executed SQL statement results.
	 */
	protected void printResultSet(ResultSet rs) throws SQLException
	{
		if(rs.isBeforeFirst()) {
	    	printHeader(rs);
	    }
	    printRows(rs);
	}
	
	/*
	 * Figure out what type of SQL statement we have and call the appropriate method for that type.
	 */
	protected void runSQL(String sql) throws Exception {
		if(echoStmt) {
			consolePrintln(sql);
		}
		if(isCommand(sql, "SELECT")) {
			runQuery(sql);
		}
		else if(isCommand(sql, "UPDATE") || isCommand(sql, "DELETE") || isCommand(sql, "INSERT")) {
			runUpdate(sql);
		}
		else {
			runStatement(sql);
		}
	}
	
	/*
	 * Run a query SQL statement.
	 */
	protected void runQuery(String sql) throws Exception {
		ResultSet rs = null;
		try {
			Statement stmt = getStatement();
			rs = stmt.executeQuery(sql);
			printResultSet(rs);
		}
		finally {
			closeDBResources(null, stmt, rs);
		}
	}
	
	/*
	 * Run a general SQL statement.
	 */
	protected void runStatement (String sql) throws Exception {
		ResultSet rs = null;
		Statement stmt = null;
		try{
			stmt = getStatement();
			stmt.execute(sql);
			if((rs=stmt.getResultSet()) != null) {
				printResultSet(rs);
			}
		}
		finally {
			closeDBResources(null, stmt, rs); 
		}
	}
	
	/*
	 * Run SQL statement update, insert, delete.
	 */
	protected void runUpdate(String sql) throws Exception {
		int count = 0;
		try {
			Statement stmt = getStatement();
			count = stmt.executeUpdate(sql);
			consolePrintln(count + " rows affected.");
		}
		finally {
			closeDBResources(null, stmt, null);
		}
	}
	
	/*
	* Print the header for a statement resultset.
	*/
	protected void printHeader(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
	    int columnCharsTotal = 0;
	    // Print a header.
	    StringBuilder lineOut = new StringBuilder();
	    for(int i = 1; i <= columnsNumber; i++) {
	    	if (i > 1) {
	    		lineOut.append(" | ");
	    		columnCharsTotal++;
	    	}
	    	// It would be nice to have a better table.
	    	int displaySize = rsmd.getColumnDisplaySize(i) + 1;
	    	// Text columns on MariaDB are really really wide. 
	    	// We check for < 0 for h2-1.4.199.jar.
	    	if(displaySize > maxColumnWidth || displaySize <= 0) {
	    		displaySize = maxColumnWidth;
	    	}
	    	lineOut.append(getPaddedValue(rsmd.getColumnLabel(i), displaySize, " "));
	    	columnCharsTotal += displaySize;
	    }
	    String separator = getPaddedValue("", columnCharsTotal, "-");
	    consolePrintln(separator);
	    consolePrintln(lineOut.toString());
	    consolePrintln(separator);
	  }
	  
	  /*
	  * Print the rows of a result set.
	  */
	  protected void printRows(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		StringBuilder lineOut = new StringBuilder();
		int columnsNumber = rsmd.getColumnCount();
	    // Print the rows.
	    long numRows = 0;
	    while (rs.next()) {
	    	numRows++;
	    	lineOut = new StringBuilder();
	        for (int i = 1; i <= columnsNumber; i++) {
	            if (i > 1) {
	            	lineOut.append(" | ");
	            }
	            int displaySize = rsmd.getColumnDisplaySize(i) + 1;
	            if(displaySize > maxColumnWidth || displaySize <= 0) {
		  		  		displaySize = maxColumnWidth;
		    			}
	            lineOut.append(getPaddedValue(rs.getString(i), displaySize, " "));
	        }
	        consolePrintln(lineOut.toString());
	    }
	    consolePrintln("Rows " + numRows);
	  }
}