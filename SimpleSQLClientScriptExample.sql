set
set comment #
# This script has been tested on postgreSQL and MariaDB.
# SQL statements will run against an Alfresco DB. 
set echostmt true
# Offer some good advice. Could be set in a connection properties 
# file as an environment reminder.
set prompt ProductionDoNotBreakIt!!>

# Empty lines are not a problem.


# This can look messy due to column width.
select * from alf_node limit 3;
# Scrunch it down to fit better.
set width 10
select * from alf_node limit 3;
# Change the comment to C/Java style.
set comment //
// Now we can terminate statements with a $
set delimiter $
// The DB is not going to be happy about $ as a delimiter in a statement.
set strip false
select * from alf_node limit 3$ // Comments go from start to to end of line.
// So we will strip the $ delimiter from the sql statements.
set strip true
// Leading and trailing space are not a problem.
   	select * from alf_node limit 3  $ 
// This will throw and error but not exit.
select phlegm from character$
// We can exit on error.
set errorexit true
select phlegm from character$
// Due to the error exit we don't get here.
quit
